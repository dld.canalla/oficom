<?php /* */ ?>
<script>
function scrollWin() {
    javascript:history.go(-1);return false;
}
</script>
<?php get_header(); ?>
<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
<div class="no-hero">
  <!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
  <div class="container" id="single-page">
    <div class="" id="post-roW">
<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
<div class="movile d-block d-sm-none my-4">
  <div class="row">


  <div class="col align-self-center">
  <span class="date"><?php echo get_the_date('F'); echo ' ' ; echo get_the_date('d'); echo ','; echo ' ' ; echo get_the_date('Y')?></span>
  </div>
  <div class="col align-self-center">
  <p  class="mb-0 pr-2 float-right d-flex align-items-center regresar-btn"  onclick="scrollWin()" style="color:#000!important; cursor:pointer">Regresar<span class="icon-arrow-thin-left pl-2"></span></p>
  </div>
  </div>
</div>
  <div class="col-12 pr-5 my-4 d-none d-sm-none d-md-block d-lg-block d-xl-block">  
     <p  class="mb-0 pr-3 float-right d-flex align-items-center regresar-btn"  onclick="scrollWin()" style="color:#000!important; cursor:pointer">Regresar<span class="icon-arrow-thin-left pl-2"></span></p>
  </div>
  <div class="">
  <div class="col-md-2  d-flex justify-content-center d-none d-sm-none d-md-block d-lg-block d-xl-block" >
  <?php $enlace = get_the_permalink();?>
  <ul  class="list-unstyled redes-flotantes">
    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $enlace;?>  target="_blank"><i class="fab fa-facebook" style="    color: #767676;font-size: 1em;"></i></a></li>
    <li class="my-3"><a href="https://twitter.com/home?status=Visita%20este%20post%0A%0A<?php echo $enlace;?>" target="_blank"><i class="fab fa-twitter" style="    color: #767676;font-size: 1em;"></i></a></li>
    <li><a href="" target="_blank"><i class="fab fa-linkedin-in" style="    color: #767676;font-size: 1em;"></i></a></li>
  </ul>
  </div>
  <div class="col-sm-12 col-md-8 px-0 offset-md-2">
  <div  id="contenido-single" class="mb-3">
  <span class="date d-none d-sm-none d-md-block d-lg-block d-xl-block"><?php echo get_the_date('F'); echo ' ' ; echo get_the_date('d'); echo ','; echo ' ' ; echo get_the_date('Y')?></span>
    <h1><?php the_title();?></h1>
    <?php if ( has_post_thumbnail() ) : ?>
    <div class="imagen-post my-5" style="background-image: url('<?php the_post_thumbnail_url();?>'); ">  </div>
              <?php else: ?>
              <div class="imagen-post my-5" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/auto.png')"></div>
              <?php endif; ?>
            


    <p class="contenido"><?php the_post();the_content();?></p>
</div>
<div class="row  d-flex align-items-center mt-5">
  <div class="col-3">
  <h4>Etiquetas</h4>
</div>
  <div class="col-9">
  <div class="row" id="tags">
 <?php
$posttags = get_the_tags();
if ($posttags) {
  foreach($posttags as $tag) {
    echo "<p class='mb-0 mr-3'>";
    echo   $tag->name .' ';
    echo "</p>"; 
  }
}
?></div>
  </div>
</div>
  </div>
  </div>
    </div>

<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
    </div>
  </div>
</div>
<?php get_template_part('banner'); ?>
<?php get_footer(); ?>
<script>
$(window).scroll(function() {
  var scroll = $(window).scrollTop();
  if (scroll < 500) {
    $(".redes-flotantes").css("opacity","1");
    $(".redes-flotantes").css("transition","all 300ms ease-in-out");
  }else if(scroll < 700) {
    
    $(".redes-flotantes").css("opacity",".6");
    $(".redes-flotantes").css("transition","all 300ms ease-in-out");
  } else if(scroll < 800) {
    
    $(".redes-flotantes").css("opacity",".5");
    $(".redes-flotantes").css("transition","all 300ms ease-in-out");
  }else{
    $(".redes-flotantes").css("opacity","0");
    $(".redes-flotantes").css("transition","all 300ms ease-in-out");
  }
});
</script>