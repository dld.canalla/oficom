<?php

function getCategory()
    {
        $query = $this->db->get_where('trainings_category', array('cat_status' => '1'));        
        return $query->result();
    }
     function getTrainingsName($id)
    {
        $query = $this->db->get_where('trainings_name', array('training_cat_id' =>$id,'training_title_status' => '1'));        
        return $query->result();
    }
// Hook the widget initiation and run our function
add_action( 'widgets_init', 'add_Widget_Support' );
// add_action( 'after_setup_theme', 'woocommerce_support' );
// function woocommerce_support() {
//     add_theme_support( 'woocommerce' );
// }

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
    } else {
    $excerpt = implode(" ",$excerpt);
    } 
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}

    #---------------------------------------------------------------------------#
    #                  Scripts and Styles                                       #
    #---------------------------------------------------------------------------#
    function equipo_comerciales() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'bootstrapLocal', get_stylesheet_directory_uri() . '/assets/style/css/bootstrap.css' );
    wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/assets/style/css/main.css' );
    wp_enqueue_style( 'icon', get_stylesheet_directory_uri() . '/assets/style/css/style.css' );

     wp_enqueue_script('ScrollConfig', get_stylesheet_directory_uri() . '/assets/js/scrollR.js');
    wp_enqueue_script('jQuery', "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js");
    wp_enqueue_script('main', get_stylesheet_directory_uri() . '/assets/js/main.js' );
    wp_enqueue_script('anime', get_stylesheet_directory_uri() . '/assets/js/anime.js' );
    wp_enqueue_script('configanime', get_stylesheet_directory_uri() . '/assets/js/configAnime.js' );
    wp_enqueue_script('Poppers', "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js");

    }
    add_action( 'wp_enqueue_scripts', 'equipo_comerciales' );
    #-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_#
    #-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-Menu Active-_-_-_-_-_-_-_-_-_-_#
    #-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_#
    register_nav_menus( array (
    'menu_principal' => __('Menu Principal', 'Equipos comerciales')
    ));
    #---Nuevo Menu Cus---#
    function vince_check_active_menu( $menu_item ) {
        $actual_link = ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if ( $actual_link == $menu_item->url ) {
            return ' active';
        }
        return '';


    }

    // function getPostID(){
    //     $post = $wp_query->ID;
    //     return $post->ID;
    // }

/**
 * Define the action and give functionality to the action.
 */
function productos_action() {
    do_action( 'productos' );
  }
/*Woocomer*/

  /**
   * Register the action with WordPress.
   */
 add_action( 'productos_action', 'productos_action_example' );
 function productos_action_example() {
   echo 'This is a custom action hook.';
 }
    #---------------------------------------------------------------------------#
    # Hide Adminbar                                                             #
    #---------------------------------------------------------------------------#

    show_admin_bar(false);
    #-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_#
    #-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-Imgenes Destacada-_-_-_-_-_-_-_-_-_-_#
    #-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_#
    if ( function_exists( 'add_theme_support' ) ) {
        add_theme_support( 'post-thumbnails' );
     }
    add_image_size('hero', 1600, 800, true);
    #---------------------------------------------------------------------------#
    # Add Thumbnails Support                                                    #
    #---------------------------------------------------------------------------#
  
    #Texto Truncate#
    add_theme_support( 'post-thumbnails'); 
    function get_excerpt($limit, $source = null){
       
        if($source == "content" ? ($excerpt = get_the_content()) : ($excerpt = get_the_excerpt()));
        $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
        $excerpt = strip_shortcodes($excerpt);
        $excerpt = strip_tags($excerpt);
        $excerpt = substr($excerpt, 0, $limit);
        $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
        $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
        $excerpt = $excerpt.'...';
        return $excerpt ;
        
    }
    function custom_short_excerpt($excerpt){
        $limit = 80;
    
        if (strlen($excerpt) > $limit) {
            return substr($excerpt, 0, strpos($excerpt, ' ', $limit));
        }
    
        return $excerpt;
    }
    
    add_filter('the_excerpt', 'custom_short_excerpt');
?>
