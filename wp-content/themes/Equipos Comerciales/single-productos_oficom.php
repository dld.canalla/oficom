<?php /* */ ?>
<script>
function scrollWin() {
    javascript:history.go(-1);return false;
}
</script>
<?php get_header(); ?>
<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
<div class="no-hero d-none d-sm-none d-md-block d-lg-block d-xl-block" >
  <!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
  <div class="container" id="single-page">
    <div class="row" >
<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
  <div class="col-12 pr-5 my-4">  
     <p  class="mb-0 pr-3 float-right d-flex align-items-center regresar-btn"  onclick="scrollWin()" style="cursor:pointer;color:#000!important">Regresar<span class="icon-arrow-thin-left pl-2"></span></p>
  </div>
  <div class="col-md-2 text-center" >
  <?php $enlace = get_the_permalink();?>
  <ul  class="list-unstyled redes-flotantes">
    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $enlace;?>  target="_blank"><i class="fab fa-facebook" style="    color: #767676;font-size: 1em;"></i></a></li>
    <li class="my-3"><a href="https://twitter.com/home?status=Visita%20este%20post%0A%0A<?php echo $enlace;?>" target="_blank"><i class="fab fa-twitter" style="    color: #767676;font-size: 1em;"></i></a></li>
    <li><a href="" target="_blank"><i class="fab fa-linkedin-in" style="    color: #767676;font-size: 1em;"></i></a></li>
  </ul>
  </div>
  <div class="col-md-8 px-0">
    <!----->
  <div  id="productos-single" class="mb-3">
    <div class="row">
      <div class="col-sm-12 col-md-6 h-100">
<div class="top d-flex justify-content-center">
<?php if ( has_post_thumbnail() ) : ?>
<img src="<?php the_post_thumbnail_url();?>" alt="">
      <?php else: ?>
         <div class="imagen-productos my-5" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/auto.png')"></div>
              <?php endif; ?>
</div>
<style>
.blokes{
  padding: 10px;
}
</style>
<div class="bottom d-flex justify-content-between mt-4">
<div class="blokes">
<div class="imagenes" style="background:url(<?php the_field('imagenes_slider_imagen_1'); ?>);    background-repeat: no-repeat;
    background-size: contain;     background-position: center;     width: 82.5px;     height: 82.5px;"></div>
</div>
<div class="blokes">
<div class="imagenes" style="background:url(<?php the_field('imagenes_slider_imagen_2'); ?>);    background-repeat: no-repeat;
    background-size: contain;     background-position: center;     width: 82.5px;     height: 82.5px;"></div>
</div>
<div class="blokes">
  <div class="imagenes" style="background:url(<?php the_field('imagenes_slider_imagen_2'); ?>);    background-repeat: no-repeat;
    background-size: contain;     background-position: center;     width: 82.5px;     height: 82.5px;"></div>
</div>
</div>
      </div>
      <div class="col-sm-12 col-md-6 h-100">
 
 <h1 class="mb-0"><?php the_title();?></h1>
<p class="cuantO mb-0">$ <?php echo the_field('precio'); ?></p>
    <?php the_post();the_content();?>
<p class="pb-0"><strong>Medidas:</strong> <br>
<?php echo the_field('medidas_alto'); ?> x <?php the_field('medidas_ancho'); ?>
</p>
<p class="pb-0"><strong>Material:</strong><br> 
<?php echo the_field('datos_del_producto_material'); ?>
</p>
<p class="pb-0"><strong>Usos:</strong><br>
<?php echo the_field('datos_del_producto_usos'); ?>
</p>
<p class="pb-0"><strong>Color:</strong><br>
  <div class="color-radius" style="height: 30px;">
    <div class="color-picker1" title="Color"></div>
    <div class="color-picker2" title="Color"></div>
    <div class="color-picker3" title="Color"></div>
 
    <style>
      .cuantO{
        font-weight: 300!important;
    font-stretch: normal!important;
    line-height: 2.57!important;
    color: #0085c6!important;
    font-size: 1.5em!important;
    letter-spacing: 0.1px!important;
    padding-top: 0px!important;
      }
      .color-radius{
        display:inline-flex;
      }
      .blokes{
        display:inline-flex;
        background:#f7f7f7;
      }
      .color-picker1{
        background:<?php the_field("colores_disponibles_color_1"); ?>;
        margin-right: 4px;
        width: 30px;
         height: 30px;
         border: 1.5px solid #e1e1e1;
         border-radius:50%;  
      }
      .color-picker2{
        background:<?php the_field("colores_disponibles_color_2"); ?>;
        margin-right: 4px;
        width: 30px;
         height: 30px;
         border: 1.5px solid #e1e1e1;
         border-radius:50%;  
      }
      .color-picker3{
        background:<?php the_field("colores_disponibles_color_3"); ?>;
        margin-right: 4px;
        width: 30px;
         height: 30px;
         border: 1.5px solid #e1e1e1;
         border-radius:50%;  
      }

    </style>
  </div>
</p>
      </div>
    </div>

    
</div>
<!------------------------------------------->
  </div>
    </div>


<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
    </div>
<!------>
<div class="container-fluid" id="line_a">

</div>
<div class="container main-productos" id="shuffled">
  <div class="row ">
    <div class="col">
    <h3 style=" font-weight: bold;" class="mb-3">Productos similares</h3>
    </div>
  </div>
  <div class="row produc_ my-3">
      <?php $wpb_all_query = new WP_Query(array('post_type'=>'productos_oficom', 'post_status'=>'publish', 'posts_per_page=3' )); ?>
      <?php if ( $wpb_all_query->have_posts() ) : ?>
                                <!-- the loop -->
         <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
<!--A-->
<div class="col-sm-12 col-md-4  my-4">

  <div class="card-deck">
   <div class="card">
   <div class="imagen-hero text-center">
          
              <div  class="card-img-top " style="background-image: url('<?php the_post_thumbnail_url();?>')">  </div>
      
             </div>
             <a href="<?php the_permalink(); ?>" class="">
             <div class="card-body p-4">
                <h5 class="card-title" title="<?php the_title(); ?>" style="font-size: 1.2rem;"><?php the_title(); ?></h5>
                <p class="card-text"><?php the_excerpt(); ?></p>
                <p class="mb-0"><span> Ver más</span></p>
              </div>
    
   </div>
  </div>
  </a>
</div>
<!--B-->

<?php endwhile; ?>
   <!-- end of the loop -->
   <?php wp_reset_postdata(); ?>
      <?php else : ?>
      <p>
        <?php _e( 'Sorry, no posts matched your criteria.' ); ?>
      </p>
      <?php endif; ?>  
</div>
</div>
    <!----------->
  </div>
</div>
<!------------------------------>

<div class="no-hero container" id="mobile">
  <div class="row">
    <div class="col">
    <p  class="mb-0 pr-3 float-right d-flex align-items-center regresar-btn"  onclick="scrollWin()" style="    cursor: pointer;
    font-weight: bold;
    color: #000!important;
    text-transform: uppercase;">Regresar<span class="icon-arrow-thin-left pl-2"></span></p>
    </div>
  </div>
  <div class="row">
      <div class="col">
      <h1 class="" style="    font-size: 3rem; font-weight: 600;"><?php the_title();?></h1>
      </div>
  </div>
  <div class="row">
      <div class="col m-4">
        <div class="top d-flex justify-content-center p-5" style="background: #fafafa;">
        <?php if ( has_post_thumbnail() ) : ?>
        <img src="<?php the_post_thumbnail_url();?>" alt="" width="">
           <?php else: ?>
            <div class="imagen-productos my-5" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/auto.png')"></div>
           <?php endif; ?>
        </div>
      </div>
  </div>
  <div class="row">
    <div class="col">
    <?php the_post();the_content();?><br>
      <p class="pb-0"><strong>Medidas:</strong> <br> 
      <?php echo the_field('medidas_alto'); ?> x <?php the_field('medidas_ancho'); ?>
      </p><br> 
      <p class="pb-0"><strong>Material:</strong><br> 
      <?php echo the_field('datos_del_producto_material'); ?>
      </p><br> 
      <p class="pb-0"><strong>Usos:</strong><br>
      <?php echo the_field('datos_del_producto_usos'); ?>
    </p>
    </div>
  </div>

<div class="container-fluid" id="line_a"></div>
<div class="container main-productos" id="shuffled">
  <div class="row ">
    <div class="col-12">
    <h3 style=" font-weight: bold;" class="mb-3">Productos similares</h3>
    </div>
  </div>
  <div class="row produc_ my-3">
      <?php $wpb_all_query = new WP_Query(array('post_type'=>'productos_oficom', 'post_status'=>'publish', 'posts_per_page=3' )); ?>
      <?php if ( $wpb_all_query->have_posts() ) : ?>
                                <!-- the loop -->
         <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
<!--A-->
<div class="col-sm-12 col-md-4  my-4">

  <div class="card-deck">
   <div class="card">
   <div class="imagen-hero text-center">
          
              <div  class="card-img-top " style="background-image: url('<?php the_post_thumbnail_url();?>')">  </div>
      
             </div>
             <a href="<?php the_permalink(); ?>" class="">
             <div class="card-body p-4">
                <h5 class="card-title" title="<?php the_title(); ?>" style="font-size: 1.2rem;"><?php the_title(); ?></h5>
                <p class="card-text"><?php the_excerpt(); ?></p>
                <p class="mb-0"><span> Ver más</span></p>
              </div>
    
   </div>
  </div>
  </a>
</div>
<!--B-->

<?php endwhile; ?>
   <!-- end of the loop -->
   <?php wp_reset_postdata(); ?>
      <?php else : ?>
      <p>
        <?php _e( 'Sorry, no posts matched your criteria.' ); ?>
      </p>
      <?php endif; ?>  
</div>
</div>
</div>
<!---------------->
<style>
.mobile p{
  line-height: 1.94;

letter-spacing: 0.5px;

color: #767676;
}
</style>
<!--------------------------------->
<?php get_template_part('banner'); ?>
<?php get_footer(); ?>
<script>
$(window).scroll(function() {
  var scroll = $(window).scrollTop();
  if (scroll < 300) {
    $(".redes-flotantes").css("opacity","1");
    $(".redes-flotantes").css("transition","all 300ms ease-in-out");
  }else if(scroll < 400) {
    
    $(".redes-flotantes").css("opacity",".6");
    $(".redes-flotantes").css("transition","all 300ms ease-in-out");
  } else if(scroll < 500) {
    
    $(".redes-flotantes").css("opacity",".5");
    $(".redes-flotantes").css("transition","all 300ms ease-in-out");
  }else{
    $(".redes-flotantes").css("opacity","0");
    $(".redes-flotantes").css("transition","all 300ms ease-in-out");
  }
});
</script>