  <!DOCTYPE html>
  <html lang="es">
  	<head>
  		<meta charset="UTF-8">

  		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		  <title><?php bloginfo('name'); ?> &raquo; <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
  		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/brand/16.png" sizes="16x16">
  		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/brand/32.png" sizes="32x32">
			<?php wp_head(); ?>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
		</head>
  	<body id="body">
		<div class="d-none d-sm-none d-md-block d-lg-block d-xl-block">
      <?php get_template_part('navbar'); ?>
    </div>
    <div class=" d-block d-sm-none">
      <?php get_template_part('navbar-mob'); ?>
    </div>
  		<!---->
