<?php /*
  Template Name: Aviso
  Template Post Type: page
  */ ?>
<?php get_header(); ?>
<div class="no-hero">
<div class="container aviso">
  <div class="row">
   <div class="col-6 offset-3">
   <h1 >Aviso de privacidad</h1>
    <p class="text-center cuerpo pb-3">
    Vestibulum faucibus, neque quis rutrum viverra, enim turpis finibus tellus, a gravida dui risus eget lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed bibendum pretium augue, eu bibendum risus luctus vel. Suspendisse commodo libero nec massa pretium hendrerit. Aliquam dolor erat, malesuada ut mattis in, ultrices sed urna. In facilisis odio sed blandit convallis. Duis mollis rhoncus aliquet. Nullam et magna sed neque sagittis ultrices. Cras ultrices convallis ornare. Nulla lobortis risus odio, in efficitur nisi tristique nec.
<br><br>
    Vestibulum tincidunt, metus nec consectetur ultricies, mauris massa fringilla justo, a sodales lorem purus ac neque. Donec nec mi semper, dapibus neque id, faucibus massa. Curabitur vitae rutrum quam. Pellentesque a scelerisque leo. Maecenas aliquam sed ante nec tincidunt. Suspendisse tincidunt velit ac diam elementum efficitur. Praesent sodales, augue sed mattis auctor, justo massa euismod velit, at vulputate arcu odio a arcu.
    <br><br>
    Quisque quis viverra massa, vel elementum dui. Vivamus ut enim rhoncus, auctor diam quis, venenatis arcu. Sed eu lorem consequat, luctus justo quis, efficitur enim. Nulla et libero egestas urna porta consequat ac quis orci. Pellentesque tristique est sed justo pulvinar, porttitor ultrices mauris maximus. Praesent feugiat euismod finibus. Aliquam erat volutpat. Phasellus interdum dui in nulla aliquam, in vestibulum nisl vehicula. Praesent feugiat augue et libero gravida, et molestie eros commodo. Ut at varius sem. In vitae justo vel ipsum imperdiet sagittis bibendum at augue.
    <br><br>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vulputate odio nec massa faucibus convallis. Curabitur consequat ipsum id ipsum sodales, sed volutpat justo imperdiet. Nam ex est, viverra at interdum vel, ullamcorper in neque. Praesent in ex sapien. Quisque eu laoreet lorem. Suspendisse lobortis arcu vel tempus convallis. Nam pulvinar neque tellus, a dictum neque efficitur non. Nullam ut nisi sollicitudin, ornare eros suscipit, placerat dui.
    <br><br>
    Vestibulum faucibus, neque quis rutrum viverra, enim turpis finibus tellus, a gravida dui risus eget lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed bibendum pretium augue, eu bibendum risus luctus vel. Suspendisse commodo libero nec massa pretium hendrerit. Aliquam dolor erat, malesuada ut mattis in, ultrices sed urna. In facilisis odio sed blandit convallis. Duis mollis rhoncus aliquet. Nullam et magna sed neque sagittis ultrices. Cras ultrices convallis ornare. Nulla lobortis risus odio, in efficitur nisi tristique nec.
    <br><br>
    Vestibulum tincidunt, metus nec consectetur ultricies, mauris massa fringilla justo, a sodales lorem purus ac neque. Donec nec mi semper, dapibus neque id, faucibus massa. Curabitur vitae rutrum quam. Pellentesque a scelerisque leo. Maecenas aliquam sed ante nec tincidunt. Suspendisse tincidunt velit ac diam elementum efficitur. Praesent sodales, augue sed mattis auctor, justo massa euismod velit, at vulputate arcu odio a arcu.
    <br><br>
    Quisque quis viverra massa, vel elementum dui. Vivamus ut enim rhoncus, auctor diam quis, venenatis arcu. Sed eu lorem consequat, luctus justo quis, efficitur enim. Nulla et libero egestas urna porta consequat ac quis orci. Pellentesque tristique est sed justo pulvinar, porttitor ultrices mauris maximus. Praesent feugiat euismod finibus. Aliquam erat volutpat. Phasellus interdum dui in nulla aliquam, in vestibulum nisl vehicula. Praesent feugiat augue et libero gravida, et molestie eros commodo. Ut at varius sem. In vitae justo vel ipsum imperdiet sagittis bibendum at augue.
    <br><br>
    Cras nec dignissim justo. Etiam tristique magna lectus, nec gravida risus tincidunt egestas. Nam congue tellus lacus, at gravida nisi egestas vitae. Mauris pulvinar faucibus ultricies. Duis commodo, tellus a laoreet imperdiet, arcu libero pulvinar dui, et pretium nisl dui posuere libero. Morbi 
    </p>
    <a href="<?php bloginfo('url');?>" class="mt-4 principalAzul" id="btn-error">Regresar</a>
   </div>
  </div>
</div>
</div>