<?php /*
Template Name: Blog_test
Template Post Type: post, page, product
*/ ?>
<?php get_header(); ?>
<!---->
<div class="no-hero">
<!---->
<div class="container-fluid pt-4 d-none d-sm-none d-md-block d-lg-block d-xl-block" style="background-color: #fafafa;">
<div class="container">
  <div class="row">

    <div class="col">
      <div class="row">
     <div class="col my-2">
     <h2>Filtro</h2>
     </div>
      </div>
  <form action="#" class="">
  <div class="row mb-5 my-3">
    <div class="col-sm-12 col-md-3  px-2 align-self-center">
    <select name="Categorías" id="" class="">
    <option value="" disabled selected hidden>Categorías </option>
            <?php
$categories = get_categories();
foreach($categories as $category) {
echo '<option class="col-md-12"><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></option>';
}
?>
          </select>
    </div>
    <div class="col-sm-12 col-md-3  px-2 align-self-center mx-5">
    <?php
wp_dropdown_categories('show_count=0&selected=-1&hierarchical=1&depth=1&hide_empty=0&exclude=1&show_option_none=Main Categories&name=main_cat');
?>
<select name="sub_cat" id="sub_cat" disabled="disabled">

</select>

    </div>


    <div class="col-sm-12 col-md-3  px-2 align-self-center">
      <?php
/*
Plugin Name: Frontend AJAX Sub-Category Dropdown
Plugin URI: http://khaledsaikat.com
Description: Load sub-category dropdown based on parent category by ajax call
Version: 0.1
Author: Khaled Hossain
Author URI: http://khaledsaikat.com
*/
/**
 * Add shortcode [ajax-dropdown] to any post or page
 */
if ( ! class_exists( 'frontendAjaxDropdown' ) ):
    class frontendAjaxDropdown
    {
        /**
         * Loading WordPress hooks
         */
        function __construct()
        {
            /**
             * Add shortcode function
             */
            add_shortcode( 'ajax-dropdown', array($this, 'init_shortocde') );
            /**
             * Register ajax action
             */
            add_action( 'wp_ajax_get_subcat', array($this, 'getSubCat') );
            /**
             * Register ajax action for non loged in user
             */
            add_action('wp_ajax_nopriv_get_subcat', array($this, 'getSubCat') );
        }
        /**
         * Show parent dropdown for wordpress category and loaded necessarry javascripts
         */
        function init_shortocde()
        {
            wp_dropdown_categories(
                'name=main_cat&selected=-1&hierarchical=1&depth=1&hide_empty=0&show_option_none=All Categories'
            );
            ?>
            <script type="text/javascript">
                (function($){
                    $("#main_cat").change(function(){
                        $("#sub_cat").empty();
                    	$.ajax({
                    		type: "post",
                            url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                            data: { action: 'get_subcat', cat_id: $("#main_cat option:selected").val() },
                    		beforeSend: function() {$("#loading").fadeIn('slow');},
                    		success: function(data) {
                                $("#loading").fadeOut('slow');
                                $("#sub_cat").append(data);
                    		}
                    	});
                    });
                })(jQuery);
            </script>

            <div id="loading" style="display: none;">Loading...</div>
            <div id="sub_cat"></div>
            <?php
        }
        /**
        * AJAX action: Shows dropdown for selected parent
        */
        function getSubCat()
        {
            wp_dropdown_categories(
                "name=sub_cat&selected=-1&hierarchical=1&depth=1&hide_empty=0&child_of={$_POST['cat_id']}"
            );
            die();
        }
    }
endif;
new frontendAjaxDropdown(); ?>
      <input type="text" name="keyword" placeholder="Por Palabra clave">
    </div>

    <div class="col-sm-12 col-md-1 align-self-center d-flex justify-content-center ml-5" style="text-align: right;">
    <input type="button" value="" class="icon-search la_lupa">
      <span class="icon-search align-self-center" style="    position: absolute; color: white; "></span>
    </div>
  </div>
  </form>
  </div>

</div>
  </div>
</div>
<!----------Mobile------>
<div class="container-fluid py-4 d-block d-sm-none" style="    background-color: #fff;
    border-bottom: solid 0.5px rgba(151, 151, 151, 0.23);">
  <div class="row ">
    <div class="col">
    <p  class="mb-0 pr-3 float-right d-flex align-items-center regresar-btn" style="font-weight: 600;color:#000!important; cursor:pointer">
      FILTRAR<span class="icon-experiment pl-2"></span>
    </p>
    </div>
  </div>
</div>


<!-----------_________--->
<div class="container pb-5" id="blog-padding">
<div class="row">
<div class="col my-3">
<h2>Blog</h2>
</div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-12">
      <div class="row">
    <?php
$temp     = $wp_query;
$wp_query = null;
$wp_query = new WP_Query();
$wp_query->query('posts_per_page=6' . '&paged=' . $paged);
while ($wp_query->have_posts()):
    $wp_query->the_post();
?>
    <div class="col-sm-12 col-md-4  my-4" >
      <div class="card-deck" >
      <a href="<?php
    the_permalink();
?>" class="post-blog-link" style=" text-decoration: none;">
      <div class="card">
      <?php
    if (has_post_thumbnail()):
?>

 <div class="card-img-top" style="background-image: url('<?php
        echo the_post_thumbnail_url();
?>')">  </div>
    <?php
    else:
?>
         <div class="card-img-top" style="background-image: url('<?php
        echo get_template_directory_uri();
?>/assets/images/auto.png')">  </div>
      <?php
    endif;
?>
    <div class="px-4 py-3 text_blog_aux">
      <div class="card-body">
      <p class=""><span style="color:#000;"> <?php
    echo get_the_date('d');
    echo ' · ';
    echo get_the_date('m');
    echo ' · ';
    echo get_the_date('Y');
?></span></p>
      <h5 class="card-title" title="<?php
    the_title();
?>"><?php
    the_title();
?></h5>
       <div class="text-p" style="    height: 140px; ">
          <?php
    the_excerpt();
?>...
          </div>
    <p class="mb-0" style="margin-top:0.75rem"><span style="color:#000;    position: absolute;
    bottom: 24px;
">Leer más</span></p>
    </div>
    </div>
  </div>
    </a>
  </div>
</div>
<?php
endwhile;
?>
        </div>
        </div>
      </div>
      <!------>
      <div class="nav-previous alignleft"><?php next_posts_link( '<span class="icon-chevron-left"></span>' ); ?></div>
<div class="nav-next alignright"><?php previous_posts_link( '<span class="icon-chevron-right"></span>' ); ?></div>
    </div>
</div>
<?php get_template_part('banner'); ?>
<?php get_footer(); ?>
<script>

</script>
