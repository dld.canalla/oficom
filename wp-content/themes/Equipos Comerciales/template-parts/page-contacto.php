<?php /*
  Template Name: Contacto
  */ ?>
  <?php get_header(); ?>
  <?php get_header(); ?>
  <div id="contacto">
    <div class="container h-100" style="    overflow: hidden;">
      <div class="row">
        <div class="col-sm-10 col-md-12">
          <a href="<?php bloginfo('url');?>"><p class="adios pt-4" style="cursor: pointer">CERRAR<span class="pl-2 icon-cross"></span></p></a>
        </div>
      </div>
        <div class="row h-100" id="text-contacto">
        <div class="col-sm-12 col-md-4 align-self-center">
          <h1 class="alt mb-3">We’re ready, Let’s talk.</h1>
          <p class="mb-4">
          Contáctenos para saber más sobre cómo podemos ayudarlo mejor.
          </p>
          <p class="mb-4">
         <strong> Dirección </strong><br>
          Av. Tecnológico 60 Nte. Centro. <br>
          Querétaro, Querétaro
          </p>
       
          <p class="mb-4">
            
            <a href="https://goo.gl/maps/PwTSa9pHKHx" target="_blank" class="principal">
            Cómo llegar
          </a><br>
          </p>
          <p class="mb-4">
         <strong> Teléfonos </strong><br>
          442-000-0000 <br>
          442-000-0000
          </p>
         
          <p>Siguenos</p>
          <ul class="list-inline mb-0">
            <a href="#">
            <li class="list-inline-item">
            <i class="fab fa-facebook"></i>
            </li></a>
            <a href="#">
              <li class="list-inline-item pl-4">
            <i class="fab fa-instagram"></i>
            </li></a>
            <a href="#">
              <li class="list-inline-item pl-4">
            <i class="fab fa-twitter"></i>
            </li></a>
            <a href="#">
              <li class="list-inline-item pl-4">
            <i class="fab fa-linkedin-in"></i>
            </li></a>
          </ul>
        </div>
        <div class="col-sm-12 col-md-8 d-flex justify-content-center align-self-center" id="noverenmovile">
        <div class="backcicle-contacto"></div>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home/contacto.png" class="img-fluid mt-5 pt-4" alt="">
        </div>
        </div>
    </div>
  </div>