<?php /*
Template Name: Tienda
Template Post Type: post, page, product
*/ ?>
<?php get_header(); ?>
<!----------------------->

<?php if ( have_posts() ): ?>

<?php woocommerce_content(); ?>

<?php endif; ?>



<!------------------------>
<?php
get_template_part('banner');
?>
<?php
get_footer();
?>