<?php /*
  Template Name: Nosotros
  Template Post Type: post, page, product
  */ ?>
  <?php get_header(); ?>
<!---->
<div class="no-hero">

<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
<div class="container" id="">
    <div class="" id="">
      <div class="col-md-8 offset-md-2 px-0 py-5" id="nosotros-eq">
        <div class="row">
        <div class="col-12">
          <h1 class="nosotros_titulo load_hidden">Nosotros</h1>
          </div>
         <div class="col-12">
         <img class="img-fluid my-5 nosotros_img load_hidden" src="<?php echo get_template_directory_uri(); ?>/assets/images/about.jpg" alt="">
          <p class="nosotros_parrafo load_hidden" >
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sollicitudin leo eu sapien congue, vitae auctor magna tristique. Curabitur ac ullamcorper neque, aliquam dignissim felis. I
          Integer vitae cursus arcu. Vestibulum porta massa et mattis vestibulum. 
          Ut feugiat nibh ac volutpat feugiat. Etiam at est blandit, gravida ligula quis, bibendum nunc. Proin pellentesque dui eu velit tempor dapibus.
          In eget mi sit amet ex gravida interdum egestas in eros. Praesent consectetur, purus ut dictum luctus, diam sapien congue odio, semper lobortis ligula enim in libero. Aenean eu dignissim ante. Sed massa nunc, fringilla sed lectus sed, 
          consequat vestibulum diam. Vestibulum pellentesque neque diam, tristique accumsan nibh varius nec. Curabitur dui ex, consequat ut porttitor at, luctus nec diam. Nunc semper sed sapien nec hendrerit. Curabitur vulputate cursus neque sed vehicula. 
          Pellentesque at porttitor magna. Duis quis diam convallis, ornare magna at, pellentesque diam.
          </p>
          </div>
          <!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
    </div>
  </div>
</div>
</div>
  <!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
  <div class="container-fluid">
    <div class="row">
    <section>
             <div class="banner-nosotros" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/banner-nos.jpg')">
            <p id="dentro_la_imagen" class="nosotros_titulo h-100 d-flex align-items-end d-flex justify-content-around" style=" ">
            “Duis quis diam convallis, ornare<br> magna at,  pellentesque diam”</p> 
            </div>
      </section>
    </div>
  </div>
<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
<div class="container" id="">
    <div class="" id="">
      <div class="col-md-8 offset-md-2 px-0 py-5" id="nosotros-eq">
        <div class="row" style="    margin-bottom: 180px;">
        <div class="col-12">
          <h1 class="my-5 load_hidden nosotros_titulo">Filosofia</h1>
          </div>
          <div class="col-12">
          <p class="load_hidden nosotros_parrafo">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sollicitudin leo eu sapien congue, vitae auctor magna tristique. Curabitur ac ullamcorper neque, aliquam dignissim felis. I
          Integer vitae cursus arcu. Vestibulum porta massa et mattis vestibulum. 
          Ut feugiat nibh ac volutpat feugiat. Etiam at est blandit, gravida ligula quis, bibendum nunc. Proin pellentesque dui eu velit tempor dapibus.
          In eget mi sit amet ex gravida interdum egestas in eros. Praesent consectetur, purus ut dictum luctus, diam sapien congue odio, semper lobortis ligula enim in libero. Aenean eu dignissim ante. Sed massa nunc, fringilla sed lectus sed, 
          consequat vestibulum diam. Vestibulum pellentesque neque diam, tristique accumsan nibh varius nec. Curabitur dui ex, consequat ut porttitor at, luctus nec diam. Nunc semper sed sapien nec hendrerit. Curabitur vulputate cursus neque sed vehicula. 
          Pellentesque at porttitor magna. Duis quis diam convallis, ornare magna at, pellentesque diam.
          </p>
          <img class="img-fluid my-4 load_hidden nosotros_img" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-nos-2.jpg" alt="">
          <img class="img-fluid my-4 load_hidden nosotros_img" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-nos-3.jpg" alt="">
          </div>
          <!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
    </div>
  </div>
</div>
</div>
  <!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
</div>
<?php get_template_part('banner'); ?>
<?php get_footer(); ?>