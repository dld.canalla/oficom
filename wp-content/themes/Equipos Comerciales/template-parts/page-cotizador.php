<?php /*
  Template Name: Cotizador
  Template Post Type: post, page, product
  */ ?>
  <?php get_header(); ?>
  <!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
<div class="no-hero">
  <!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
<form action="#" class="" role="document">
    <div id="formluario-cotizador" class="py-1">
    <div class="container h-100">
      
      <div class="row  align-items-center">
        <div class="col-12 pt-4 align-items-center mb-3 mt-3">
          <h2 class="my-3">Cotizador</h2>
          <p>Suspendisse lobortis arcu vel tempus convallis. Nam pulvinar neque tellus, 
            a dictum neque efficitur non. Nullam ut nisi sollicitudin, ornare eros suscipit, placerat dui.</p>
        </div>

    <div class="col-12">
      <div class="row">
        <div class="col-sm-12 col-md-6 p-2 mb-3 px-2">
          <input type="text" name="nombre" placeholder="Nombre Completo">
        </div>
        <div class="col-sm-12 col-md-6 p-2 mb-3 px-2">
            <input type="text" name="empresa" placeholder="Empresa">
        </div>
        <div class="col-sm-12 col-md-4 p-2 mb-3 px-2">
          <input type="mail" name="mail" placeholder="Correo electrónico">
        </div>
        <div class="col-sm-12 col-md-4 p-2 mb-3 px-2">
        <input type="text" name="Ubicación" placeholder="Ubicación">
        </div>
        <div class="col-sm-12 col-md-4 p-2 mb-3 px-2">
        <input type="number" name="tel" placeholder="Teléfono">
        </div>
      
      </div>
    </div>
    </div>
    </div>
    </div>
<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
<!--/--/--/--Lista Productos--/--/--/--/--/--/-->
<div class="container my-5 ">
  <div class="row">
    <div class="col-sm-12 col-md-12">
        
          <div class="col-12 titulo-lista">
          <div class="row texto-titulo">    

              <div class="col-6">
                <p class="pl-5 mb-0">Nombre del producto</p>
              </div>
              <div class="col-6">
                <p class="pr-5 mb-0">Cantidad</p>
              </div>
          </div>
        </div>
    </div>
  </div>
  <!--------------->
  <div class="row">
    <div class="col my-5">
      <button class="btn-azul float-right" type="submit" value="Cotizar">Cotizar</button>
    </div>
  </div>
</div>



<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
</form>


<!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
</div>
  <!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
<?php get_template_part('banner'); ?>
<?php get_footer(); ?>