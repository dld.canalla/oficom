<?php /*
  Template Name: Proximamente
  Template Post Type: post, page, product
  */ ?>
  <?php get_header(); ?>

<div id="proximamente" >
    <div class="centrar">
    <p>PROYECTOS</p>
     <h1>PRÓXIMAMENTE</h1>
     <img src="<?php echo get_template_directory_uri(); ?>/assets/images/404/proximamente.png" class="img-fluid" alt="">
     <a href="<?php bloginfo('url');?>" class="mt-4 principal" id="btn-error">Regresar</a>
  </div>
  </div>
  <?php get_footer(); ?>