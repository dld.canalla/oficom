<?php
/*
Template Name: Home
Template Post Type: post, page, product
*/
?>
 <?php
get_header();
?>
<div class="home">
  <!---->
<div class="ho-hero  d-none d-sm-none d-md-block d-lg-block d-xl-block">
<div class="container pb-4 pt-3">
  <div class="row" style="">
    <div class="col mt-5">
  <div class="row  h-100">
    <div class="col-sm-12 col-md-6 align-self-center">
    <h1 class="alt load_hidden" style="font-size: 3rem;">
Lorem ipsum dolor
sit amet, consectetur
</h1>
<p class="sub_text load_hidden"><span>Duis arcu urna, ultrices vel odio arcu urna, ultrices vel odio</span></p>
        <a href="<?php
the_permalink(8);
?>" class="principal ir_a_ver load_hidden">
            Ver más
          </a>
    </div>
    <div class="col-sm-12 col-md-6 d-flex justify-content-center">
    <div class="whitecicle"></div>

<div id="contenedor-slider_2" class="contenedor-slider_2">
 <div id="slider_2" class="slider_2">
    <section class="slider__section_2"><img src="<?php echo the_field('slider_imagen_principal'); ?>" class="slider__img_2"></section>
    <section class="slider__section_2"><img src="<?php echo the_field('slider_imagen_secundaria'); ?>" class="slider__img_2"></section>
    <section class="slider__section_2"><img src="<?php echo the_field('slider_imagen_tercera'); ?>" class="slider__img_2"></section>
    <section class="slider__section_2"><img src="<?php echo the_field('slider_imagen_cuarta'); ?>" class="slider__img_2"></section>
  </div>

 </div>
    </div>
</div>
</div>
</div>
</div>
<div class="container-fluid mt-5" style="    bottom: 0;
    position: absolute;  overflow: hidden;">
<div class="row h-100" >
    <div class="col-md-1 offset-md-1 text-center px-0 btn_scroll_sti">
      <div id="btn-prev" class="btn-prev ml-0">&#62; </div>

      <div id="btn-next" class="btn-next ml-0" style="margin-top: 8px">&#60;</div>
    </div>
    <div class="col-md-4 offset-md-4">
<div id="contenedor-slider" class="contenedor-slider">
 <div id="slider" class="slider">
    <section class="slider__section"><img src="<?php echo the_field('slider_imagen_principal'); ?>" class="slider__img"></section>
    <section class="slider__section"><img src="<?php echo the_field('slider_imagen_secundaria'); ?>" class="slider__img"></section>
    <section class="slider__section"><img src="<?php echo the_field('slider_imagen_tercera'); ?>" class="slider__img"></section>
    <section class="slider__section"><img src="<?php echo the_field('slider_imagen_cuarta'); ?>" class="slider__img"></section>
  </div>

 </div>
    </div>
  </div>

  </div>
  </div>
<!---Mobile---->
<div class="ho-hero h-100 d-block d-sm-none">
<div class="container pb-4 pt-3">
  <div class="row">
    <div class="col mt-5">
  <div class="row  h-100">
    <div class="col-sm-12 col-md-6 align-self-center">
    <h1 class="alt load_hidden" >
Lorem ipsum dolor
sit amet, consectetur
</h1>
<p class="sub_text load_hidden"><span>Duis arcu urna, ultrices vel odio arcu urna, ultrices vel odio</span></p>
        <a href="<?php
the_permalink(8);
?>" class="principal ir_a_ver load_hidden">
            Ver más
          </a>
    </div>
    <div class="col-sm-12 col-md-6 d-flex justify-content-center">


<div id="contenedor-slider_3" class="contenedor-slider_3">
 <div id="slider_3" class="slider_3">
    <section class="slider__section_3"><img src="<?php echo the_field('slider_imagen_principal'); ?>" class="slider__img_3"></section>
    <section class="slider__section_3"><img src="<?php echo the_field('slider_imagen_secundaria'); ?>" class="slider__img_3"></section>
    <section class="slider__section_3"><img src="<?php echo the_field('slider_imagen_tercera'); ?>" class="slider__img_3"></section>
    <section class="slider__section_3"><img src="<?php echo the_field('slider_imagen_cuarta'); ?>" class="slider__img_3"></section>
  </div>

 </div>
    </div>
</div>
</div>
</div>
</div>
<div class="container-fluid mt-5 pt-5 btn_scroll_movile">
<div class="row h-100">
    <div class="col-md-1 offset-md-1 text-center px-0 btn_scroll_sti">
      <div id="btn-prev_M" class="btn-prev ml-0">&#62; </div>

      <div id="btn-next_M" class="btn-next ml-0" style="margin-top: 8px">&#60;</div>
    </div>

  </div>

  </div>
  </div>
<!---->
<div class="ec-filtro d-none d-sm-none d-md-block d-lg-block d-xl-block">
<div class="container h-100">

  <div class="row  align-items-center">
    <div class="col-12 pt-4 align-items-center mb-3 mt-3">
    <h2 class="titulo_S load_hidden">Filtrar productos</h2>
    </div>

<div class="col-12">
<form action="#" class="">
  <div class="row">
    <div class="col-sm-12 col-md-4 p-2 mb-3 px-2">
    <select name="categoria" required>
    <option value="" disabled selected hidden>Categorias</option>
      <option value="uno">Volvo XC90</option>
      <option value="dos">Saab 95</option>
      <option value="tres">Mercedes SLK</option>
      <option value="cuatro">Audi TT</option>
    </select>
    </div>
    <div class="col-sm-12 col-md-4 p-2 mb-3 px-2">
    <select name="Subcategorias " required>
      <option value="" disabled selected hidden>Subcategorias </option>
      <option value="uno">Volvo XC90</option>
      <option value="dos">Saab 95</option>
      <option value="tres">Mercedes SLK</option>
      <option value="cuatro">Audi TT</option>
    </select>
    </div>
    <div class="col-sm-12 col-md-4 p-2 mb-3 px-2">
    <select name="Subcategorias " required>
      <option value="" disabled selected hidden>Espacio</option>
      <option value="uno">Volvo XC90</option>
      <option value="dos">Saab 95</option>
      <option value="tres">Mercedes SLK</option>
      <option value="cuatro">Audi TT</option>
    </select>
    </div>

    <div class="col-sm-12 col-md-4 p-2 px-2 auxiliar_buscador">
    <select name="Subcategorias " required>
      <option value="" disabled selected hidden>Marcas</option>
      <option value="uno">Volvo XC90</option>
      <option value="dos">Saab 95</option>
      <option value="tres">Mercedes SLK</option>
      <option value="cuatro">Audi TT</option>
    </select>
    </div>
    <div class="col-sm-12 col-md-7 p-2 pl-2 ">
    <input type="text" name="Keyword" placeholder="Buscas por palabra clave ">
    </div>
    <div class="col-sm-12 col-md-1 align-self-center d-flex justify-content-center d-block d-sm-none mt-4 mb-3">
    <button class="btn-azul-full" type="submit" name="submit" value="Buscar">Buscar</button>
    </div>
    <div class="col-sm-12 col-md-1 align-self-center d-flex justify-content-center" id="buscador_lupa" style="text-align: right;">
      <input type="button" value="" class="icon-search la_lupa">
      <span class="icon-search"></span>

    </div>
  </div>
  </form>
</div>
</div>
</div>
</div>
<!---->
<div class="ho-about align-items-center">
  <div class="container h-100">
    <div class="row d-flex align-items-center d-flex justify-content-center h-100">
      <div class="col-sm-12 col-md-6 d-flex justify-content-center">
      <div class="backcicle load_hidden"></div>
      <img src="<?php
echo get_template_directory_uri();
?>/assets/images/home/silla.png" class="img-fluid mt-5 pt-2 load_hidden imagen_silla" alt="">
      </div>
      <div class="col-sm-12 col-md-6">
        <h2 class="titulo_S load_hidden">Nosotros</h2>
        <p class="parrafo_p load_hidden">Curabitur odio nulla, malesuada in venenatis ut,
          ultricies eu ex. Praesent aliquet at magna quis
          scelerisque. Fusce dictum elementum nisi sit amet auctor.</p>
          <a href="<?php
the_permalink(51);
?>" class="principalAzul btn_ver load_hidden">
            Ver más
          </a>
      </div>
    </div>
  </div>
</div>
<!---->
<div class="ho-blog align-items-center">
  <div class="container h-100">
    <div class="row">
     <div class="col-12">
     <h2 class="titulo_S load_hidden">Blog</h2>
     </div>
    </div>
    <div class="row">
    <div class="col-sm-12 col-md-12">
      <div class="row">
    <?php
$temp     = $wp_query;
$wp_query = null;
$wp_query = new WP_Query();
$wp_query->query('posts_per_page=3' . '&paged=' . $paged);
while ($wp_query->have_posts()):
    $wp_query->the_post();
?>
    <div class="col-sm-12 col-md-4  my-4" >
      <div class="card-deck" >
      <a href="<?php
    the_permalink();
?>" class="post-blog-link">
      <div class="card">
      <?php
    if (has_post_thumbnail()):
?>

 <div class="card-img-top" style="background-image: url('<?php
        echo the_post_thumbnail_url();
?>')">  </div>
    <?php
    else:
?>
         <div class="card-img-top" style="background-image: url('<?php
        echo get_template_directory_uri();
?>/assets/images/auto.png')">  </div>
      <?php
    endif;
?>
    <div class="px-4 py-3 text_blog_aux">
      <div class="card-body">
      <p class=""><span style="color:#000;"> <?php
    echo get_the_date('d');
    echo ' · ';
    echo get_the_date('m');
    echo ' · ';
    echo get_the_date('Y');
?></span></p>
      <h5 class="card-title" title="<?php
    the_title();
?>"><?php
    the_title();
?></h5>
       <div class="text-p" style="    height: 140px; ">
          <?php
    the_excerpt();
?>...
          </div>
    <p class="mb-0" style="margin-top:0.75rem"><span style="color:#000;    position: absolute;
    bottom: 24px;
">Leer más</span></p>
    </div>
    </div>
  </div>
    </a>
  </div>
</div>
<?php
endwhile;
?>
        </div>
        </div>
      </div>
      <!------>
    </div>
  </div>
  <!----BLOG---->

<!---->
</div>
<?php
get_template_part('banner');
?>
<?php
get_footer();
?>
<script>
$(window).scroll(function() {
  var scroll = $(window).scrollTop();
  if (scroll < 500) {

    document.getElementById("navBar").style.background="transparent";
    document.getElementById("logo-w").style.display="block";
    document.getElementById("logo-o").style.display="none";
    document.getElementById("navBar").style.transition="all 300ms ease-in-out";

    $(".Menu_Text").css("color", "#fff").css("transition","all 300ms ease");
    $(".menu-icon i").css("color", "#fff").css("transition","all 300ms ease");
    $(".contacto_btnL").css("color", "#fff").css("transition","all 300ms ease");
    $(".icon-shopping-bag").css("color", "#fff").css("transition","all 300ms ease");
    $("#contacto_aux").css("border-left", "1px solid rgba(255, 255, 255, 0.32)").css("border-right", "1px solid rgba(255, 255, 255, 0.32)");

  }else {

    document.getElementById("navBar").style.background="#fff";
    document.getElementById("logo-w").style.display="none";
    document.getElementById("logo-o").style.display="block";
    $(".Menu_Text").css("color", "#0085c6" ).css("transition","all 300ms ease");
    $(".menu-icon i").css("color", "#0085c6" ).css("transition","all 300ms ease");
    $(".contacto_btnL").css("color", "#0085c6" ).css("transition","all 300ms ease");
    $(".icon-shopping-bag").css("color", "#0085c6").css("transition","all 300ms ease");
    $("#contacto_aux").css("border-left", "1px solid rgba(0, 132, 200, 0.32)").css("border-right", "1px solid rgba(0, 132, 200, 0.32)");
    document.getElementById("navBar").style.transition="all 300ms ease-in-out";
  }
});
</script>
