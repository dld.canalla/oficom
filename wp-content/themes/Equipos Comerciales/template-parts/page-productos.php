<?php /*
  Template Name: Productos
  Template Post Type: post, page, product
  */ ?>
<?php get_header(); ?>
<!---->
<div class="no-hero">
<!---->
<!---->
<!----->

<div class="ec-filtro d-none d-sm-none d-md-block d-lg-block d-xl-block">
<div class="container h-100">
  
  <div class="row  align-items-center">
    <div class="col-12 pt-4 align-items-center mb-3 mt-3">
      <h2>Filtrar productos</h2>
    </div>

<div class="col-12">
<form action="#" class="">
  <div class="row">
    <div class="col-sm-12 col-md-4 p-2 mb-3 px-2" id="categorias_d">
      
    <select name="categoria" required>
    <option value="" disabled selected hidden>Categorias</option>
      <option value="uno">Volvo XC90</option>
      <option value="dos">Saab 95</option>
      <option value="tres">Mercedes SLK</option>
      <option value="cuatro">Audi TT</option>
    </select>
    </div>
    <div class="col-sm-12 col-md-4 p-2 mb-3 px-2">
    <select name="Subcategorias " required>
      <option value="" disabled selected hidden>Subcategorias </option>
      <option value="uno">Volvo XC90</option>
      <option value="dos">Saab 95</option>
      <option value="tres">Mercedes SLK</option>
      <option value="cuatro">Audi TT</option>
    </select>
    </div>
    <div class="col-sm-12 col-md-4 p-2 mb-3 px-2">
    <select name="Subcategorias " required>
      <option value="" disabled selected hidden>Espacio</option>
      <option value="uno">Volvo XC90</option>
      <option value="dos">Saab 95</option>
      <option value="tres">Mercedes SLK</option>
      <option value="cuatro">Audi TT</option>
    </select>
    </div>

    <div class="col-sm-12 col-md-4 p-2 px-2 auxiliar_buscador">
    <select name="Subcategorias " required>
      <option value="" disabled selected hidden>Marcas</option>
      <option value="uno">Volvo XC90</option>
      <option value="dos">Saab 95</option>
      <option value="tres">Mercedes SLK</option>
      <option value="cuatro">Audi TT</option>
    </select>
    </div>
    <div class="col-sm-12 col-md-7 p-2 px-2">
    <input type="text" name="Keyword" placeholder="Buscas por palabra clave ">
    </div>
    <div class="col-sm-12 col-md-1 align-self-center d-flex justify-content-center d-block d-sm-none mt-5 mb-3">
    <button class="btn-azul-full" type="submit" name="submit" value="Buscar">Buscar</button>
    </div>
    <div class="col-sm-12 col-md-1 align-self-center d-flex justify-content-center" id="buscador_lupa" style="text-align: right;">
      <input type="button" value="" class="icon-search la_lupa">
      <span class="icon-search"></span>
    
    </div>
  </div>
  </form>
</div>
</div>
</div>
</div>
<!-------------------->
<!----------Mobile------>
<div class="container-fluid py-4 d-block d-sm-none" style="    background-color: #fff;
    border-bottom: solid 0.5px rgba(151, 151, 151, 0.23);">
  <div class="row ">
    <div class="col">
    <p  class="mb-0 pr-3 float-right d-flex align-items-center regresar-btn" style="font-weight: 600;color:#000!important; cursor:pointer">
      FILTRAR<span class="icon-experiment pl-2"></span>
    </p>
    </div>
  </div>
</div>
<!--Main-Productos-->
 


<form action="">
  <div class="main-productos">
    <div class="container">
      <div class="row mb-5">
      <div class="col-12">
      <h1>Productos</h1>
      </div>
      </div>
      <div class="row produc_ my-3">
      <?php $wpb_all_query = new WP_Query(array('post_type'=>'productos_oficom', 'post_status'=>'publish',  )); ?>
      <?php if ( $wpb_all_query->have_posts() ) : ?>
                                <!-- the loop -->
         <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
<!--A-->
<div class="col-sm-12 col-md-4  my-4">
<a href="<?php the_permalink(); ?>" class="">
  <div class="card-deck">
   <div class="card">
   <div class="imagen-hero text-center">
              <div class=" d-flex justify-content-center d-flex align-items-center">
      <label>
       
              <div class="back-heart d-flex justify-content-center d-flex align-items-center">
              <input type="checkbox"  id="<?php the_post_thumbnail_url();?>" descripcion="<?php the_field('texto_apoyo_corta'); ?>" value="<?php the_title(); ?>" name="productos"> 
              <span class="icon-heart"></span>
            
              </label>
              </div>
              </div>
              <div  class="card-img-top " style="background-image: url('<?php the_post_thumbnail_url();?>')">  </div>
      
             </div>
            
             <div class="card-body p-4">
                <h5 class="card-title" title="<?php the_title(); ?>" style="font-size: 1.2rem;"><?php the_title(); ?></h5>
                <p class="card-text"><?php the_excerpt(); ?></p>
                <p class="mb-0"><span> Ver más</span></p>
              </div>
    
   </div>
  </div>
  </a>
</div>
<!--B-->

<?php endwhile; ?>
   <!-- end of the loop -->
   <?php wp_reset_postdata(); ?>
      <?php else : ?>
      <p>
        <?php _e( 'Sorry, no posts matched your criteria.' ); ?>
      </p>
      <?php endif; ?>  
</div>

      <!---->
      <!---->

  </div>
</div>
<!---->
</form>
<!---->
</div>

<?php get_template_part('banner'); ?>
<?php get_footer(); ?>
