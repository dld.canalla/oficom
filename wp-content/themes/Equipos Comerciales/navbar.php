<?php if (is_page(6) ||is_page(11)): ?>
<div class="container-fluid my-0 py-2" id="navBar">
  <div class="row">

<div class="container" >
  <div class="row">
    <div class="col-12">
   <div class="row align-items-center py-1 d-flex justify-content-between">
   <div class="col-md-2 col-sm-2 logo_nav_mov">
   <a href="<?php the_permalink(6)?>">
          <img id="logo-o" src="<?php echo get_template_directory_uri(); ?>/assets/images/brand/logo.svg" class="img-fluid" alt="">
          <img id="logo-w" src="<?php echo get_template_directory_uri(); ?>/assets/images/brand/logo-w.svg" class="img-fluid" alt="">
</a>
        </div>



  <div class="col-sm-0 col-md-1  offset-sm-6 offset-md-6 d-flex justify-content-center aux_color" id="carrito-cotizador">
         <span  id="shopping-bag" class="icon-shopping-bag"></span>
         <p class="ml-3" id="contador-pro" > </p>
      </div>
    
      <div class="col-md-1 col-sm-1 aux_color d-flex justify-content-center" >
        
         <a class="contacto_btnL px-3" id="contacto_aux" href="<?php the_permalink(49)?>">Contacto</a>
        
      </div>

      <div class="col-md-1 col-sm-1 d-flex justify-content-center aux_color " >
       
          <div class="row btn_menu_ " style="cursor: pointer">
          <p class="mb-0 Menu_Text">MENÚ</p>
              <div class="menu-icon">
                     <i class="fas fa-bars"></i>
              </div> 
          </div>
        
      </div>

     
    
      
   </div>
    </div>
  </div>
</div>
  </div>
</div>
<div class="menu-lado-izq">
<div class="menu ">
  <div class="data">
    <ul>
    <li><a href="<?php the_permalink(6)?>">Home</a></li>
    <li><a href="<?php the_permalink(8)?>">Productos</a></li>
    <li><a href="<?php the_permalink(51)?>">Nosotros</a></li>
    <li><a href="<?php the_permalink(22)?>">Blog</a></li>
    <li><a href="<?php the_permalink(11)?>">Proyectos</a></li>
    
    <li> <p class="pt-4">Siguenos</p></li>
    </ul>
          <ul class="list-inline mb-0">
            <a href="#">
            <li class="list-inline-item">
            <i class="fab fa-facebook" style="    color: #0085c6;font-size: .55em;"></i>
            </li></a>
            <a href="#">
              <li class="list-inline-item">
            <i class="fab fa-instagram" style="    color: #0085c6;font-size: .55em;"></i>
            </li></a>
            <a href="#">
              <li class="list-inline-item">
            <i class="fab fa-twitter" style="    color: #0085c6;font-size: .55em;"></i>
            </li></a>
            <a href="#">
              <li class="list-inline-item">
            <i class="fab fa-linkedin-in" style="    color: #0085c6;font-size: .55em;"></i>
            </li></a>
          </ul>
  </div>
</div>
</div>

<?php elseif (is_page(3)||is_404()||is_page(49)): ?>
<!--Aqui no hay-->
<?php else: ?>
<div class="container-fluid my-0 py-2" id="navBar-white">
  <div class="row">

<div class="container" >
  <div class="row">
    <div class="col-12">
   <div class="row align-items-center py-1 d-flex justify-content-between ">
   <div class="col-md-2 col-sm-2 logo_nav_mov">
   <a href="<?php the_permalink(6)?>">
          <img id="logo-o" src="<?php echo get_template_directory_uri(); ?>/assets/images/brand/logo.svg" class="img-fluid" alt="">
</a>
        </div>
    
    
        <div class="col-sm-0 col-md-1  offset-sm-6 offset-md-6 d-flex justify-content-center aux_color" id="carrito-cotizador">
         <span  id="shopping-bag" class="icon-shopping-bag"></span>
         <p class="ml-3" id="contador-pro" > </p>
      </div>
    
      <div class="col-md-1 col-sm-1 aux_color d-flex justify-content-center" >
        
      <a class="contacto_btnL px-3" id="contacto_aux" href="<?php the_permalink(49)?>">Contacto</a>
        
      </div>

      <div class="col-md-1 col-sm-1 d-flex justify-content-center aux_color " >
       
       <div class="row btn_menu_ " style="cursor: pointer">
          <p class="mb-0 Menu_Text">MENÚ</p>
              <div class="menu-icon">
                     <i class="fas fa-bars"></i>
              </div> 
          </div>
        
      </div>
     
   
   </div>
    </div>
  </div>
</div>
  </div>
</div>

<div class="menu-lado-izq">
<div class="menu ">
  <div class="data">
    <ul>
    <li><a href="<?php the_permalink(6)?>">Home</a></li>
    <li><a href="<?php the_permalink(8)?>">Productos</a></li>
    <li><a href="<?php the_permalink(51)?>">Nosotros</a></li>
    <li><a href="<?php the_permalink(22)?>">Blog</a></li>
    <li><a href="<?php the_permalink(11)?>">Proyectos</a></li>
    
    <li> <p class="pt-4">Siguenos</p></li>
    </ul>
          <ul class="list-inline mb-0">
            <a href="#">
            <li class="list-inline-item">
            <i class="fab fa-facebook" style="    color: #0085c6;font-size: .55em;"></i>
            </li></a>
            <a href="#">
              <li class="list-inline-item">
            <i class="fab fa-instagram" style="    color: #0085c6;font-size: .55em;"></i>
            </li></a>
            <a href="#">
              <li class="list-inline-item">
            <i class="fab fa-twitter" style="    color: #0085c6;font-size: .55em;"></i>
            </li></a>
            <a href="#">
              <li class="list-inline-item">
            <i class="fab fa-linkedin-in" style="    color: #0085c6;font-size: .55em;"></i>
            </li></a>
          </ul>
  </div>

</div>
</div>

<?php endif; ?>
<script>
        $('.btn_menu_').click(function(){
            $(this).find('i').toggleClass('fa-bars fa-times')
        });
        </script>