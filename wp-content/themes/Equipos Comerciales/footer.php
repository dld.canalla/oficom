
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<?php get_template_part('cotizador-modal'); ?>
<?php wp_footer(); ?>
<?php if ( is_page(11) ): ?>

<!--Aqui no hay-->
<?php else: ?>
<footer class="footer">
  <div class="container h-100">
    <div class="row d-flex align-items-center h-100">
      <div class="col-sm-12 col-md-4 ">
      <div class="">
      <h4 class="footer_titulo load_hidden">QUERÉTARO</h4>
        <p class="footer_addre load_hidden">
        Av. Tecnológico 60 Nte. Centro.<br>
        Querétaro, Querétaro.<br>
        <a href="tel:4422152015">+52 (442) 215 2015 / 16</a>
        </p>
      </div>

        
      </div>
      <div class="col-sm-12 col-md-2 offset-sm-0 offset-md-5">
      <h4 class="footer_titulo load_hidden">Siguenos</h4>
        <ul class="list-inline d-flex justify-content-start">
          <li class="list-inline-item mr-4 load_hidden social_1">
            <a href="#" target="_blank"><i class="fab fa-facebook-square"></i></a>
          </li>
          <li class="list-inline-item load_hidden social_2">
            <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </div>

</footer>
<div class="container-fluid Subfooter">
  <div class="row h-100">
      <div class="col-sm-12 col-md-4 offset-md-1 align-self-center">
        <p class="tipo_sm">© Oficom derechos reservados</p>
        </div>
        <div class="col-sm-12 col-md-2 offset-sm-0 offset-md-4 align-self-center">
        <p class="tipo_sm"><a href="<?php echo the_permalink(3)?>">Aviso de privacidad </a></p>
        </div>
    </div>
  </div>


  <?php endif ?>
  <script>
$(document).ready(function(){
  //almacenar slider en una variable
var slider = $('#slider');
//almacenar botones
var siguiente = $('#btn-next');
var anterior = $('#btn-prev');

var siguiente_M = $('#btn-next_M');
var anterior_M = $('#btn-prev_M');

//mover ultima imagen al primer lugar
$('#slider .slider__section:last').insertBefore('#slider .slider__section:first');
//mostrar la primera imagen con un margen de -100%
slider.css('margin-left', '-'+90+'%');

function moverD() {
	slider.animate({
		marginLeft:'-'+90+'%'
	} ,300, function(){
		$('#slider .slider__section:first').insertAfter('#slider .slider__section:last');
    slider.css('margin-left', '-'+90+'%');
    $('#slider_2 .slider__section_2:first').insertAfter('#slider_2 .slider__section_2:last');
		slider.css('margin-left', '-'+40+'%');
	});
}

function moverI() {
	slider.animate({
    marginLeft:0
	} ,300, function(){
		$('#slider .slider__section:last').insertBefore('#slider .slider__section:first');
    slider.css('margin-left', '-'+90+'%');
    $('#slider_2 .slider__section_2:last').insertBefore('#slider_2 .slider__section_2:first');
		slider.css('margin-left', '-'+40+'%');
	});
}

function autoplay() {
	interval = setInterval(function(){
		moverD();
	}, 5000);
}
siguiente_M.on('click',function() {
	moverD_M();
	console.log("Siguiente");
});
anterior_M.on('click',function() {
	moverI_M();
	console.log("Antes");

});
function moverD_M() {
	slider.animate({
		marginLeft:'-'+90+'%'
	} ,300, function(){
	
    $('#slider_3 .slider__section_3:first').insertAfter('#slider_3 .slider__section_3:last');
		slider.css('margin-left', '-'+40+'%');
	});
}

function moverI_M() {
	slider.animate({
    marginLeft:0
	} ,300, function(){
	
    $('#slider_3 .slider__section_3:last').insertBefore('#slider_3 .slider__section_3:first');
		slider.css('margin-left', '-'+40+'%');
	});
}
siguiente.on('click',function() {
	moverD();
	

});

anterior.on('click',function() {
	moverI();
	

});


// autoplay();
}); 
  </script>
  <script>
  // #navBar-white
  // toggle-btn
  $( document ).ready(function() {
 
    let bandera = false;
    $( ".btn_menu_" ).click(function() {

      if(!bandera){
   
        $("#body").css("overflow", "hidden");
        $(".Menu_Text").css("color", "#0085c6" ).css("transition","all 300ms ease");
        $(".menu-icon i").css("color", "#0085c6" ).css("transition","all 300ms ease");
        $(".contacto_btnL").css("color", "#0085c6" ).css("transition","all 300ms ease");
        $(".icon-shopping-bag").css("color", "#0085c6").css("transition","all 300ms ease");
        
        bandera=true;
      }else{
       
        $("#body").css("overflow", "scroll");
        $(".Menu_Text").css("color", "#fff").css("transition","all 300ms ease");
        $(".menu-icon i").css("color", "#fff").css("transition","all 300ms ease");
        $(".contacto_btnL").css("color", "#fff").css("transition","all 300ms ease");
        $(".icon-shopping-bag").css("color", "#fff").css("transition","all 300ms ease");
        bandera=false;
      }
     
    });

      $( "#navBar-white .btn_menu_" ).click(function() {

if(!bandera){
  $("#navBar-white").css("background","rgba(255,255,255,0)").css("transition","all 300ms ease");
  $("#navBar-white").css("border-bottom","solid 0.5px rgba(151, 151, 151, 0.23)").css("transition","all 300ms ease");
 
  $("#body").css("overflow", "hidden");
  bandera=true;
}else{
  $("#navBar-white").css("background","#fff").css("transition","all 300ms ease");
  $("#navBar-white").css("border-bottom","solid 0.5px rgba(151, 151, 151, 0.23)").css("transition","all 300ms ease");
  $("#body").css("overflow", "scroll");
  
  bandera=false;
}

});
   
    $("#carrito-cotizador").click(function() {
        $("#body").css("overflow", "hidden");
       
    });
    $("#close_modal").click(function() {
      $("#body").css("overflow", "scroll");
       
    });
});
  </script>
  <script>
  
var t1 = new TimelineMax({paused: true});

  t1.to(".one", 0.1, {
    y:6,
    rotation:45,
    ease: Expo.easeInOut, 
    background: "#0085c6"
  });

  t1.to(".two", 0.1, {
    y:-6,
    rotation:-45,
    ease: Expo.easeInOut,
    delay: -0.1,
    background: "#0085c6"
  });
  t1.to(".menu", .19, {
      top: "0%",
      ease: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
      position:'fixed',
      display: 'block',
      visibility: 'visible',
      width: '50%',
      delay: 0
  });
  t1.to(".menu-lado-izq", .19, {
      top: "0%",
      ease: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
      position:'fixed',
      visibility: 'visible',
      width: '50%',
      display: 'block',
      delay: 0
  });
  t1.to("#logo-w", .1, {
  opacity: '0',
  display: 'none',
  ease: 'cubic-bezier(0.6, 0.2, 0.1, 1)'
});
t1.to("#logo-o", .2,{
  opacity: '1',
  display: 'block',
  ease: 'cubic-bezier(0.6, 0.2, 0.1, 1)'
});
t1.staggerFrom(".menu ul li", .075, {x: +180, opacity: 0, ease:'cubic-bezier(0.785, 0.135, 0.15, 0.86)'}, 0.11);


t1.reverse();


$(document).on("click", ".btn_menu_", function() {
     t1.reversed(!t1.reversed());
});
$(document).on("click", ".menu a", function() {
     t1.reversed(!t1.reversed());
});

  </script>
  <!--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/--/-->
<script>


window.sr = ScrollReveal();
//Home
sr.reveal(".alt", {
  duration: 400
});
sr.reveal(".sub_text", {
  duration: 600,
  delay:200
});
sr.reveal(".ir_a_ver", {
  duration: 800,
  delay:400
});

sr.reveal(".backcicle", {
  duration: 600,
  delay: 100
});
sr.reveal(".imagen_silla", {
  duration: 600,
  delay: 650
});

sr.reveal(".titulo_S", {
  duration: 600,
  delay: 100
});
sr.reveal(".parrafo_p", {
  duration: 600,
  delay: 650
});
sr.reveal(".btn_ver", {
  duration: 600,
  delay: 650
});

//////////Banner
sr.reveal(".banner_titulo", {
  duration: 650,
  delay: 450,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
});
sr.reveal(".banner_parrafo", {
  duration: 600,
  delay: 400,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
});
sr.reveal(".banner_btn", {
  duration: 600,
  delay: 800
});

//////////Nosotros
sr.reveal(".nosotros_titulo", {
  duration: 650,
  delay: 450,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
});
sr.reveal(".nosotros_img", {
  duration: 800,
  delay: 400,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
});
sr.reveal(".nosotros_parrafo", {
  duration: 800,
  delay: 400,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
});
sr.reveal(".nosotros_btn", {
  duration: 800,
  delay: 800
});
// //////////Footer
sr.reveal(".footer_titulo", {
  duration: 800,
  origin: 'top',
  distance: '20px',
  delay: 200,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
});
sr.reveal(".footer_addre", {
  duration: 800,
  origin: 'bottom',
  distance: '20px',
  delay: 400,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
});
sr.reveal(".footer_addre", {
  duration: 800,
  origin: 'bottom',
  distance: '20px',
  delay: 400,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
});
sr.reveal(".social_1", {
  duration: 800,
  delay: 400,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
});
sr.reveal(".social_2", {
  duration: 800,
  delay: 500,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
});


</script>

    <script type="text/javascript">
        $(document).ready(function(){
          var n = " ";
          $("#close_modal").click(function() {
              n.length = 0;
              $("#lista-de-productos").html("");
             
            });
          $("#borrar-items").click(function() {
            alert("Lo va a borrar");
            console.log("ksks");
          });
          
            $("#shopping-bag").click(function() {
              var  listapro = $("input[name='productos']:checked");
                var p=$("#nombre");
                var img=$("#imagen_pro");
               
             
                $.each($("input[name='productos']:checked"), function() {
                  var nombre = $(this).val();
                  var url_img = $(this).attr('id');
                  var descrip = $(this).attr('descripcion');
                  n = '<div class="col-12 crearPro">'
                        +'  <div class="row "><!----->'
                        +'    <div class="col-md-2 px-0">'
                        +'      <div class="back-img h-100 d-flex justify-content-center p-3"  id="imagen_pro">'
                        +'        <img src="'+url_img+'" alt="" class="" style="width: auto;  height: 100px;">'
                        +'      </div>'
                        +'    </div>'
                        +'    <div class="col-md-4 d-flex align-items-center pl-5">'
                        +'  <div class="row "><!----->'
                        +'  <div class="col "><!----->'
                        +'      <p id="nombre" class="mb-0">'+nombre+'</p>'
                        +'      <p id="descip" class="mb-0">'+descrip+'</p>'
                        +'       </div>'
                        +'       </div>'
                        +'    </div>'
                        +'    <div class="col-md-2 d-flex align-items-center">'
                        +'      <a class="px-2  align-items-center align-self-center d-flex justify-content-center" id="down1" href="#" onclick="updateSpinner(this,this.nextElementSibling);">-</a>'
                        +'      <input type="number" min="1" max="999" class="qty-val mx-3" name="quantity[01]" id="product_quantity_01" value="1">'
                        +'      <a class="px-2  align-items-center align-self-center d-flex justify-content-center" id="up1" href="#"  onclick="updateSpinner(this,this.previousElementSibling);">+</a>'
                        +'    </div>'
                        +'    <div class="col-md-1 offset-md-3 align-self-center d-flex justify-content-center">'
                        +'      <label class="mb-0 adios_lista d-flex justify-content-center" >'
                        +'        <span class="icon-cross adios-delista" id="borrar-items"></span>'
                        +'      </label>'
                        +'    </div>'
                        +'  </div>'
                        +'</div>';
                        $("#lista-de-productos").append(n);
                });      
            }); 





                     
       /*BLOQUE DE CODIGO ***/


           $("input[name='productos']").change(function() {
            var listapro = $("input[name='productos']:checked");
            var cuantos = listapro.length;
            if(!listapro.length == 0){
              $.each($("input[name='productos']:checked"), function() {

             
              console.log("Si hay");
              $("#shopping-bag").css("opacity","1");
              $("#shopping-bag").css("transition","all 300ms ease");
              // $(".icon-heart").css("color", "#F5A623");
              $("#contador-pro").css("opacity","1");
              $("#contador-pro").html(cuantos);
              $("#contador-pro").css("transition","all 350ms ease");
            });
            }else{
              console.log("No hay");
              $("#contador-pro").html("");
              $("#shopping-bag").css("opacity","0");
              $("#shopping-bag").css("transition","all 300ms ease");
              // $(".icon-heart").css("color", "#0085c6");
              $("#contador-pro").css("opacity","0");
              $("#contador-pro").html(cuantos);
              $("#contador-pro").css("transition","all 350ms ease");
            }
          });
        });
    </script>

<script>
  var ver_shop = document.getElementById("shopping-bag").addEventListener("click", elmodal);
  var modal_shop = document.getElementById("elmodal_style");
  var close_m = document.getElementById("close_modal").addEventListener("click", elmodal);   
  var producto = document.querySelectorAll("input[type='checkbox']");  
  let estas = false;
  function elmodal(){
      if(!estas){
      window.scrollTo(0, 0 );
    setTimeout(function() {
        modal_shop.classList.add("baja-modal");
        window.addEventListener('scroll', noscroll);
      }, 200);

        estas = true;
      }else{
        window.removeEventListener('scroll', noscroll);
        modal_shop.classList.remove("baja-modal");
  
        estas = false;
      }
  }

  function noscroll() {
  // window.scrollTo( 0, 0 );
}

</script>
</body>
</html>